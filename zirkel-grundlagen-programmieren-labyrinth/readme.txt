Klasse: 5. & 6.

Zirkel f�r Leute ohne jegliche Programmiererfahrung. Nutzt einfache Pseudosprache und anschauliche Labyrinthe. Am Ende Algorithmus, der einen in jedem Labyrinth (nicht Irrgarten) zum Ziel f�hrt.
Zeigt, wie n�tzlich Schleifen, Bedingungen und bedingte Schleifen sind und soll ein bisschen ein Gef�hl geben, wie Bedingungen zu formulieren sind und wie man Schleifen schachteln kann.
F�r den Zirkel einfach die Datei "Folien_Programmieren mit Labyrinthen und Irrg�rten" durchgehen und immer, wenn bei einer Folie oben rechts ein * dransteht, k�nnen die Kinder selbst �berlegen (oder sogar aufschreiben), was die L�sung ist, welche dann auf den folgenden Folien kommt.
Folien haben keine Animationen, kann also auch als PDF Dokument oder so genau so gut angeschaut werden.

# Überraschende Anwendungen des Zwischenwertsatzes
Skript zum Zirkel.
Alternativ: **"Das Perfekte Erd-Sandwich"**

Grundlegend basierend auf [The fix-the-wobbly-table theorem (Mathologer)](https://www.youtube.com/watch?v=aCj3qfQ68m0).

[Skript wurde nach dem Intro nie detailliert geschrieben, weil man sich easy an den Bildern entlanghangeln kann.]

Für den Zirkel werden 4 Probleme in absteigender Schwierigkeit vorgestellt und dann rückwärts (also in aufsteigender Schwierigkeit) mathematisch erklärt.

## Das Perfekte Erd-Sandwich
Wir stellen uns vor, zwei Kinder aus dem Zirkel sind auf Weltreise.
Dabei bewegen sie sich immer so, dass sie immer genau gegenüber auf der Erde sind (antipodal).
Können nie auf dem gleichen Kontinent sein, aber Pazifik ist so groß, da können sie beide gleichzeitig drinnen sein.

Jedenfalls legen Sie jetzt irgendwo jeweils eine Brotscheibe auf den Boden und erhalten ein Erd-Sandwich – ein Sandwich mit der ganzen Erde als Füllung.
Als gerade eines der Kinder zu einem saftigen Bissen ansetzt, merkt das beißende Kind entsetzt, dass die beiden Brotscheiben bei ganz verschiedenen Lufttemperaturen liegen.
Das wird kein gutes kulinarisches Erlebnis bieten, vor allem nicht für die Zähne.

Also finden sie eine solche Stelle, sodass an beiden antipodalen Punkten die Lufttemperatur gleich ist.
Doch beim erneuten Ansetzen stellt das beißende Kind fest:
Die Luftfeuchtigkeit (alternativ: Luftdruck) an den beiden Brotscheiben ist auch verschieden.
Das kann zu spröden Lippen führen, wenn man gerade reinbeißen will.

### Wir fragen uns:
auch wenn wir diese Punkte nicht finden, existieren Punkte, sodass beide Brotscheiben die gleiche Lufttemperatur und den gleichen Luftdruck haben?

### Spoiler:
- Borsuk-Ulam, aber den Namen dazu gibt es erst am Ende: Es existiert auf der Erdoberfläche immer ein Paar an antipodalen Punkten, deren Temperatur und Luftdruck gleich sind (siehe [Fixed Points (Vsauce), ab 9:31](https://www.youtube.com/watch?t=571&v=csInNn6pfT4&feature=youtu.be)), mit Erdsandwich und Pazifik.
- geht auch für alle anderen Eigenschaften, die stetig über der Erde variieren; allgemeiner auch für n-Sphären mit n vielen Eigenschaften

## Wobbly Tabble Theorem
- liegt an unebenem Boden (nehmen an, dass die Beine nicht unregelmäßig sind)
- wie fixen? -> zB etwas unter abgehobenes Tischbein legen
- können immer durch drehen fixen
  - außer wenn kantig-uneben, oder wenn Beine unregelmäßig auf flachem Boden
  - natürlich ist Tischfläche dann auch schief, aber praktisch ist Unebenheit immer gering genug, dass der Tisch dann nicht zu schief ist
- 3 Tischbeine können nicht wackeln

## Küssende Rechtecke/Quadrate (Hugging Rectangles)
- Bilder mit Homer anschauen
- [… Rest siehe Bilder]

## Heiße Suppe
- durch Stetigkeit der Temperaturänedrung können wir abwarten, bis die Suppe die richtige Temperatur hat, und bei ausreichender Messungsdichte verpassen wir den Moment auch nicht
